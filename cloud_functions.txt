# START CUSTOM BELOW THIS LINE

## sudo
for x in $(sudo -l | grep -Po "\(ALL\) NOPASSWD:\K.*" | tr -d '\n|,'); do
alias "${x/*\//}"="sudo $x";
done
for x in /usr/nexkit/bin/nk*; do
alias "${x/*\//}"="sudo $x";
done

alias vi='vim'

function _exit()              # Function to run upon exit of shell.
{
    echo -e "${BRed}HULK SMASH!!!${NC}"
    rm  ~/bashrceh
}
trap _exit EXIT

# Get account info for a domain
function ai {
    test -z "$1" && echo "Usage: ai domain" && return
    domain=$(echo "$1" | tr '[:upper:]' '[:lower:]')
    valhost=$(stat /etc/httpd/conf.d/vhost_$domain.conf 2> /dev/null);
    dig=$(dig +short $domain)
    acctinfo=$(sudo siteworx -u -n --login_domain="$domain" -c Overview --action listAccountDetails|grep -v "remaining_diskspace" |grep -v "options");
    user=$(sudo siteworx -u -n --login_domain="$domain" -c Overview --action listAccountDetails | grep username |cut -d " " -f 2);
    quotausage=$(sudo quota -g $user 2> /dev/null | tail -1 | awk '{printf "%10.3f%%  %10.3f GB  %10.3f GB",($2/$3*100),($2/1000/1024),($3/1000/1024)}' 2> /dev/null);
    echo "$acctinfo"
    echo "----------Disk Usage----------------"
    printf "%8s %12s %14s %14s\n" "Username" "Used(%)" "Used(G)" "Total(G)"
    echo "$user" "$quotausage"
    echo "------------------------------------"
    sudo whichphp $user 
    if [ -z "$valhost" ]; then
    # Get info for alias domain
    echo "Domain is alias"
    getvhost=$(grep -l "$domain" /etc/httpd/conf.d/vhost_*)
    root2=$(grep 'DocumentRoot' $getvhost | sort | uniq)
    vip2=$(grep -oE '[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+' $getvhost | sort | uniq)
    echo "$root2"
    echo "Vhost IP: $vip2"
    echo "Resolving IP: $dig"
  else
   #Get info for root domain
    root=$(find /etc/httpd/conf.d/ -type f -name "vhost_$domain.conf" -exec grep 'DocumentRoot' {} \; |sort | uniq)
    vip=$(find /etc/httpd/conf.d/ -type f -name "vhost_$domain.conf" -exec grep -oE '[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+' {} \; |sort | uniq)
    echo "$root"
    echo "Vhost IP: $vip"
    echo "Resolving IP: $dig"
  fi
}

#Dependency
function dash()
{
    for ((i=1; i<=$1; i++))
    do
        printf "-"
    done
    printf "\n"
}

#Dependency
getusr(){ pwd | sed 's:^/chroot::' | cut -d/ -f3; }

## Check users quota usage
checkquota(){
_quotaheader(){ echo; printf "%8s %12s %14s %14s\n" "Username" "Used(%)" "Used(G)" "Total(G)"; dash 51; }
_quotausage(){ printf "\n%-10s" "$1"; sudo quota -g $1 2> /dev/null | tail -1 | awk '{printf "%10.3f%%  %10.3f GB  %10.3f GB",($2/$3*100),($2/1000/1024),($3/1000/1024)}' 2> /dev/null; }
case $1 in
  -h|--help   ) echo -e "\n Usage: checkquota [--user <username>|--all|--large]\n   -u|--user user1 [user2..] .. Show quota usage for a user or list of users \n   -a|--all ................... List quota usage for all users\n   -l|--large ................. List all users at or above 80% of quota\n";;
  -a|--all    ) _quotaheader; for x in $(laccounts); do _quotausage $x; done | sort; echo ;;
  -l|--large  ) _quotaheader; echo; for x in $(laccounts); do _quotausage $x; done | sort | grep -E '[8,9][0-9]\..*%|1[0-9]{2}\..*%'; echo ;;
  -u|--user|* ) _quotaheader; shift; if [[ -z "$@" ]]; then _quotausage $(getusr); else for x in "$@"; do _quotausage $x; done; fi; echo; echo ;;
esac
}

## Give a breakdown of user's large disk objects
##Broken due to permissions
#diskhogs(){
#if [[ $@ =~ "-h" ]]; then echo -e "\n Usage: diskhogs [maxdepth] [-d]\n"; return 0; fi;
#if [[ $@ =~ [0-9]{1,} ]]; then DEPTH=$(echo $@ | grep -Eo '[0-9]{1,}'); else DEPTH=3; fi;
#echo -e "\n---------- Large Directories $(dash 51)"; du -h --max-depth $DEPTH | grep -E '[0-9]G|[0-9]{3}M';
#if [[ ! $@ =~ '-d' ]]; then echo -e "\n---------- Large Files $(dash 57)"; find . -type f -size +100000k -group $(getusr) -exec ls -lah {} \;; fi;
#echo -e "\n---------- Large Databases $(dash 53)"; du -sh /var/lib/mysql/$(getusr)_* | grep -E '[0-9]G|[0-9]{3}M';
#echo
#}

## Show backupserver and disk usage for current home directory
backupsvr(){
checkquota;
NEW_IPADDR=$(awk -F/ '/server.allow/ {print $NF}' /usr/sbin/r1soft/log/cdp.log | tail -1 | tr -d \' | sed 's/10\.17\./178\.17\./g; s/10\.1\./103\.1\./g; s/10\.240\./192\.240\./g');
ALL_IPADDR=$(awk -F/ '/server.allow/ {print $NF}' /usr/sbin/r1soft/log/cdp.log | sort | uniq | tr -d \' | sed 's/10\.17\./178\.17\./g; s/10\.1\./103\.1\./g; s/10\.240\./192\.240\./g');
if [[ $NEW_IPADDR =~ ^172\. ]]; then INTERNAL=$(curl -s http://mdsc.info/r1bs-internal); fi
_printbackupsvr(){
  if [[ $1 =~ ^172\. ]]; then
    for x in $INTERNAL; do echo -n $x | awk -F_ "/$1/"'{printf "R1Soft IP..: https://"$3":8001\n" "R1Soft rDNS: https://"$2":8001\n"}'; done
  else
    IP=$1; RDNS=$(dig +short -x $1 2> /dev/null);
    echo "R1Soft IP..: https://${IP}:8001";
    if [[ -n $RDNS ]]; then echo "R1Soft rDNS: https://$(echo $RDNS | sed 's/\.$//'):8001"; fi;
  fi
  echo
}
FIRSTSEEN=$(grep $(echo $NEW_IPADDR | cut -d. -f2-) /usr/sbin/r1soft/log/cdp.log | head -1 | awk '{print $1}');
echo "----- Current R1Soft Server ----- $FIRSTSEEN] $(dash 32)"; _printbackupsvr $NEW_IPADDR

for IPADDR in $ALL_IPADDR; do
  if [[ $IPADDR != $NEW_IPADDR ]]; then
    LASTSEEN=$(grep $(echo $IPADDR | cut -d. -f2-) /usr/sbin/r1soft/log/cdp.log | tail -1 | awk '{print $1}');
    echo "----- Previous R1Soft Server ----- $LASTSEEN] $(dash 31)"; _printbackupsvr $IPADDR
  fi;
done
}


## Lookup the DNS Nameservers on the host
nameserver(){ echo; for x in $(grep ns[1-2] ~iworx/iworx.ini | cut -d\" -f2;); do echo "$x ($(dig +short $x))"; done; echo; }

## List server IPs, and all domains configured on them.
domainips(){
echo; for I in $(ip addr show | awk '/inet / {print $2}' | cut -d/ -f1 | grep -Ev '^127\.'); do
    printf "  ${BRIGHT}${YELLOW}%-15s${NORMAL}  " "$I";
    D=$(grep -l $I /etc/httpd/conf.d/vhost_[^000_]*.conf | cut -d_ -f2 | sed 's/.conf$//');
    for x in $D; do printf "$x "; done; echo;
done; echo
}

eh_chkmail ()
{
test -z "$1" && echo "Usage: ai domain" && return
domain=$(echo "$1" | tr '[:upper:]' '[:lower:]')
sudo siteworx -u -n --login_domain=$domain -c EmailBox -a list
}

eh_dld ()
{
test -z "$1" && echo "Usage: ai domain" && return
domain=$(echo "$1" | tr '[:upper:]' '[:lower:]')
sudo siteworx -u -n --login_domain=$domain -c EmailRemotesetup -a disableLocalDelivery --domain=$domain
}

checkip ()
{
    if [[ -z $1 ]]; then
        read -ep "IP ADDRESS: " _ip;
    else
        _ip=$1;
    fi;
    for b in $_ip;
    do
        dash 30;
        printf "${b}\n";
        printf "DATE: $(date)\n";
        nslookup $b | grep addr;
        printf "http://multirbl.valli.org/lookup/${b}.html\n";
        printf "http://www.senderbase.org/lookup/ip/?search_string=${b}\n";
        printf "https://www.senderscore.org/lookup.php?lookup=${b}\n";
        for x in hotmail.com yahoo.com aol.com earthlink.net verizon.net att.net sbcglobal.net comcast.net xmission.com cloudmark.com cox.net charter.net theantihacker.com qq.com shaw.ca mac.me;
        do
            if [[ $x = "theantihacker.com" ]]; then
                printf "Office 365\n";
            else
                printf "${x}\n";
            fi;
            dash 20;
            swaks -q TO -t postmaster@$x -li $b | grep -iE "block|rdns|550|521|554";
            printf "\n";
        done;
        printf "gmail.com\n";
        swaks -4 -t iflcars.com@gmail.com -li $b | grep -iE "block|rdns|550|521|554";
        RBLS=$(curl -s http://spamwars.nexcess.net/rbls.txt);
        for r in $RBLS;
        do
            LOOKUP="$(echo $b | awk -F. '{print $4"."$3"."$2"."$1}').$r";
            LISTED="$(dig +time=2 +tries=2 +short $LOOKUP | grep -v \;)";
            REASON="$(dig +time=1 +tries=1 +short txt $LOOKUP | grep -v \;)";
            printf "$r\n";
            if [ ! -z "$LISTED" ]; then
                echo $REASON;
            fi;
            echo;
        done;
    done
}

function eh_mail_hacked () {
grep -r 'HELO' /var/qmail/queue/mess/ | awk -F: '{print $3}' | sort |uniq -c | sort -rn | head
}

function mg {
    test -z "$1" && echo "Usage: mg ip or ModSec ID" && return
    modgrep -s "$1" -f /var/log/httpd/modsec_audit.log | grep "id " | grep -aEho "9[5-8][0-9]{4}" | sort | uniq | grep -v 981176
}

## Break Magento Training
function eh_break_m2(){
# Breaks various things in Magento2
read -r -d ''  BREAK_M2_USAGE  <<'EOF'
Usage: break_m2 [TARGET]

[TARGET]: Domain of a Magento2 site on this host.
EOF

#Get the username
if [[ $# -gt 1 ]]; then
    echo "Too many arguments. Please enter a single domain name.";
    echo "$BREAK_M2_USAGE";
    return 1;
fi;
TARGET=$1;
domain=$1;
if [[ -z "$TARGET" ]]; then
    echo "Must target a domain!";
    return 1;
else
    if id "$TARGET" &>/dev/null; then
        USER="$TARGET";
    else
        REGEX=$(printf '(ServerName|ServerAlias).*\s%s(\s|$)' "$TARGET");
        VHOST_FILE=$(grep -Pl "$REGEX" /etc/httpd/conf.d/vhost_*.conf 2>/dev/null);
        if [[ -z $VHOST_FILE ]]; then
            echo "User not found. Double check your domain entry.";
            return 1;
        fi;
        if [[ "$(echo "$VHOST_FILE" | wc -l)" -gt 1 ]]; then
            echo "Multiple matching vhost files found.";
            echo "$VHOST_FILE";
            return 1;
        fi;
        USER=$(grep -Pom1 '(?<=SuexecUserGroup )\s*.*(?=\s.*)' "$VHOST_FILE");
        id "$USER" &>/dev/null || ( echo "User not found. Double check your domain entry." && echo "$BREAK_M2_USAGE" && return 1 );
    fi;
fi;

#Get the PHP version
shelluser="$USER"
phpregex="php[0-9][0-9]"
bashrcphpregex="^(\t|\ )*source.*?php[0-9][0-9].*enable(\t|\ )*$"
if [[ ! -e "/home/${shelluser}/.bashrc" ]]; then
    printf  "No bashrc found.";
elif ! grep -qoP "$bashrcphpregex" "/home/${shelluser}/.bashrc"; then
    printf "No PHP entry in bashrc found.";
else
    phpcliversion=$(grep -oP "$bashrcphpregex" "/home/${shelluser}/.bashrc" | grep -oP "$phpregex" | tail -n1)
fi

#set phppath veriable
if [[ $phpcliversion = "php74" ]]; then
    phppath=/opt/remi/php74/root/usr/bin/php
elif [[ $phpcliversion = "php73" ]]; then
    phppath=/opt/remi/php73/root/usr/bin/php
elif [[ $phpcliversion = "php72" ]]; then
    phppath=/opt/remi/php72/root/usr/bin/php
elif [[ $phpcliversion = "php71" ]]; then
    phppath=/opt/remi/php71/root/usr/bin/php
elif [[ $phpcliversion = "php70" ]]; then
    phppath=/opt/remi/php70/root/usr/bin/php
elif [[ $phpcliversion = "php56" ]]; then
    phppath=/opt/remi/php56/root/usr/bin/php
fi;

##This is a security measure
#Must create this empty touch file or script wont run
if [[ ! -e "/home/$USER/$domain/nex_testing.txt" ]]; then
    printf "Domain is not identiied as a Nexcess Testing environment. \n";
    return 1;
fi;

#Choose the things to break
read -ep "Break ElasticSearch y/N: " es;
read -ep "Disable cache  y/N: " cache;
read -ep "Break Magento emails y/N: " cron;
read -ep "Break multistore y/N: " multi;
read -ep "Break .htaccess y/N: " htaccess;

#Breaks ElasticSearch by setting incorrect host and port in Magento2
if [[ $es = 'y' ]]
then
    sudo -u $USER  $phppath /home/$USER/$domain/bin/magento config:set --scope=default --scope-code=0 catalog/search/elasticsearch7_server_hostname cg-5179-elasticsearch.us-midwest.nxcli.net >/dev/null
  sudo -u $USER  $phppath /home/$USER/$domain/bin/magento config:set --scope=default --scope-code=0 catalog/search/elasticsearch7_server_port 45553 >/dev/null
  printf "ElasticSearch settings changed. \n"
fi;

#Disables Magento 2 cache
if [[ $cache = 'y' ]]
then
  sudo -u $USER  $phppath /home/$USER/$domain/bin/magento cache:disable >/dev/null
  printf "All Magento caches disabled. \n"
fi;

#Break Magento emails by disabling Magento cron
if [[ $cron = 'y' ]]
then
  sudo -u $USER  crontab -l > $HOME/tmpcron.txt
  sed -i '/cron\:run/d' $HOME/tmpcron.txt
  if [[ ! -e "$HOME/tmpcron.txt" ]]; then
    printf "No cron changes to apply. \n";
else
    sudo rsync -a -q --chown=$USER:$USER $HOME/tmpcron.txt /home/$USER/tmpcron.txt
    sudo -u $USER  crontab /home/$USER/tmpcron.txt
    sudo -u $USER rm /home/$USER/tmpcron.txt
    printf "Magento run cron removed \n"
fi;
rm $HOME/tmpcron.txt
fi;

#Break Magento Multistore
if [[ $multi = 'y' ]]
then
  sudo -u $USER  sed -i '/MAGE_RUN_/d' /home/$USER/$domain/pub/.htaccess
  sudo -u $USER  sed -i '/MAGE_RUN_/d' /home/$USER/$domain/.htaccess
  printf "Magento Multistore MAGE_RUN_ codes removed \n"
fi;

#Break .htaccess with incorrect Allow/Deny syntax
if [[ $htaccess = 'y' ]]
then
  sudo -u $USER  sed -i '1s/^/Order Allow,Deny \n/' /home/$USER/$domain/.htaccess
  sudo -u $USER  sed -i '2s/^/Deny from 99.88.*.* \n/' /home/$USER/$domain/.htaccess
  printf "htaccess Allow/Deny rules added with incorrect syntax \n"
fi;
} 
